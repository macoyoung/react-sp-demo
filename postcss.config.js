module.exports = {
  plugins: {
    'postcss-preset-env': true,
    'postcss-px-to-viewport-8-plugin': {
      viewportWidth: 750,
      selectorBlackList: ['root']
    }
  }
};
