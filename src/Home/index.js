import React from 'react';
import { NavLink, Outlet } from 'react-router-dom';
const sty = require('./home.less');

export default function Home() {
  return (
    <div className={sty.container}>
      <div className={sty.content}>
        HOME PAGE;
        <Outlet />
      </div>

      <ul className={sty.footer}>
        <NavLink to='1'>1111</NavLink>
        <NavLink to='2'>2222</NavLink>
      </ul>
    </div>
  );
};
