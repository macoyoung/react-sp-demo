import React, { Suspense } from 'react';
import { createRoot } from 'react-dom/client';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { BrowserRouter, useRoutes, useLocation } from 'react-router-dom';
import '../html/init.css';
import routes from './routes';

const baseName = location.host.includes('localhost') ? '' : '/alex-rsp';
const AppPage = () => useRoutes(routes);

// 根结点
const App = () => {
  const location = useLocation();

  return (
    <TransitionGroup component={null}>
      <CSSTransition
        timeout={300}
        key={location.key}
        classNames='fade'
      >
        <div className='container' >
          <Suspense fallback={null}>
            <AppPage />
          </Suspense>
        </div>
      </CSSTransition>
    </TransitionGroup>
  );
};

const root = createRoot(document.getElementById('root'));

root.render(
  <BrowserRouter basename={baseName}>
    <App />
  </BrowserRouter>
);
