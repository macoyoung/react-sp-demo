import React, { lazy } from 'react';

// 主页
const Index = lazy(() => import('./Index'));

// 第二页
const Home = lazy(() => import('./Home'));

const P1 = () => <p>ppppp11111</p>;
const P2 = () => <p>pp22222</p>;

export default [
  {
    path: '/',
    element: <Index />
  },
  { // 有公共的父组件
    path: '/home',
    element: <Home />,
    children: [
      { path: '1', index: true, element: <P1 /> },
      { path: '2', element: <P2 /> }
    ]
  },
  {
    path: '*',
    element: <p>呜呜... 404</p>
  }
];
