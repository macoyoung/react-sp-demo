const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  entry: './src/app.js',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)$/,
        type: 'asset',
        parser: {
          dataUrlCondition: {
            maxSize: 4096
          }
        },
        generator: {
          filename: 'images/[name].[fullhash:8][ext]'
        }
      }
    ]
  },
  resolve: {
    alias: {
      $com: path.resolve(__dirname, 'components')
    },
    extensions: ['.js', '.json', '.jsx']
  },
  plugins: [
    new CleanWebpackPlugin({ path: path.resolve(__dirname, 'dist') }),
    new HtmlWebpackPlugin({
      hash: true,
      title: 'react-sp-page',
      template: './html/index.html',
      minify: { collapseWhitespace: true }
    })
  ]
};
